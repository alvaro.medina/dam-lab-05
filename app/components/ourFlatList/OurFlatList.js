import React, {Component} from 'react';
import {View, Text, FlatList, StyleSheet, TouchableOpacity} from 'react-native';

const DATA = [
  {id: 1, title: 'First Item'},
  {id: 2, title: 'Second Item'},
  {id: 3, title: 'Third Item'},
];

function Item({title, showAlert}) {
  return (
    <TouchableOpacity onPress={showAlert}>
      <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
}

const ListEmpty = () => {
  return (
    <View style={styles.MainContainer}>
      <Text style={{textAlign: 'center'}}>No Data Found</Text>
    </View>
  );
};

class ourFlatList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <FlatList
          data={DATA}
          renderItem={({item}) => (
            <Item title={item.title} showAlert={this.props.showAlert} />
          )}
          keyExtractor={item => item.id}
          ListEmptyComponent={ListEmpty}
        />
      </View>
    );
  }
}

export default ourFlatList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    backgroundColor: 'orange',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});